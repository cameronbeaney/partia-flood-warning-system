from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_level_over_threshold


def run():
    # Build list of stations
    stations = build_station_list()

    # Update water levels
    update_water_levels(stations)

    # Get stations over threshold
    stations_over = stations_level_over_threshold(stations, 0.8)

    for station, wl in stations_over:
        print(station.name, wl)


if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run()
