from floodsystem.analysis import polyfit
from datetime import datetime
from dateutil.tz import tzutc

def test_polyfit():
    # Run polyfit on an existing station, where we know the polynomial fits correctly

    # Hard coded test data:
    dates = [datetime(2019, 2, 23, 17, 0, tzinfo=tzutc()), datetime(2019, 2, 23, 16, 45, tzinfo=tzutc()), datetime(2019, 2, 23, 16, 30, tzinfo=tzutc()), datetime(2019, 2, 23, 16, 15, tzinfo=tzutc()), datetime(2019, 2, 23, 16, 0, tzinfo=tzutc()), datetime(2019, 2, 23, 15, 45, 1, tzinfo=tzutc()), datetime(2019, 2, 23, 15, 30, 1, tzinfo=tzutc()), datetime(2019, 2, 23, 15, 15, tzinfo=tzutc()), datetime(2019, 2, 23, 15,
0, tzinfo=tzutc()), datetime(2019, 2, 23, 14, 45, tzinfo=tzutc()), datetime(2019, 2, 23, 14, 30, 1, tzinfo=tzutc()), datetime(2019, 2, 23, 14, 15, 1, tzinfo=tzutc()), datetime(2019, 2, 23, 14, 0, tzinfo=tzutc()), datetime(2019, 2, 23, 13, 45, tzinfo=tzutc()), datetime(2019, 2, 23, 13, 30, tzinfo=tzutc()), datetime(2019, 2, 23, 13, 15, tzinfo=tzutc()), datetime(2019, 2, 23, 13, 0, tzinfo=tzutc()), datetime(2019, 2, 23, 12, 45, tzinfo=tzutc()), datetime(2019, 2, 23, 12, 30, tzinfo=tzutc()), datetime(2019, 2, 23, 12, 15, tzinfo=tzutc()), datetime(2019, 2, 23, 12, 0, tzinfo=tzutc()), datetime(2019, 2, 23, 11, 45, tzinfo=tzutc()), datetime(2019, 2, 23, 11, 30, tzinfo=tzutc()), datetime(2019, 2, 23, 11, 15, tzinfo=tzutc()), datetime(2019, 2, 23, 11, 0, tzinfo=tzutc()), datetime(2019, 2, 23, 10, 45, 1, tzinfo=tzutc()), datetime(2019, 2, 23, 10, 30, tzinfo=tzutc()), datetime(2019, 2, 23, 10, 15, 1, tzinfo=tzutc()), datetime(2019, 2, 23, 10, 0, tzinfo=tzutc()), datetime(2019, 2, 23, 9, 45, tzinfo=tzutc()), datetime(2019, 2, 23, 9, 30, tzinfo=tzutc()), datetime(2019, 2, 23, 9, 15, tzinfo=tzutc()), datetime(2019, 2, 23, 9, 0, tzinfo=tzutc()), datetime(2019, 2, 23, 8, 45, tzinfo=tzutc()), datetime(2019, 2, 23, 8, 30, tzinfo=tzutc()), datetime(2019, 2, 23, 8, 15, 1, tzinfo=tzutc()), datetime(2019, 2, 23, 8, 0, tzinfo=tzutc()), datetime(2019, 2, 23, 7, 45, tzinfo=tzutc()), datetime(2019, 2, 23, 7, 30, 1, tzinfo=tzutc()), datetime(2019, 2, 23, 7, 15, tzinfo=tzutc()), datetime(2019, 2, 23, 7, 0, tzinfo=tzutc()), datetime(2019, 2, 23, 6, 45, tzinfo=tzutc()), datetime(2019, 2, 23, 6, 30, tzinfo=tzutc()), datetime(2019, 2, 23, 6, 15, tzinfo=tzutc()), datetime(2019, 2, 23, 6, 0, tzinfo=tzutc()), datetime(2019, 2, 23, 5, 45, tzinfo=tzutc()), datetime(2019, 2, 23, 5, 30, tzinfo=tzutc()), datetime(2019, 2, 23, 5, 15, tzinfo=tzutc()), datetime(2019, 2, 23, 5, 0, tzinfo=tzutc()), datetime(2019, 2, 23, 4, 45, tzinfo=tzutc()), datetime(2019, 2, 23, 4, 30, tzinfo=tzutc()), datetime(2019, 2, 23, 4, 15, tzinfo=tzutc()), datetime(2019, 2,
23, 4, 0, tzinfo=tzutc()), datetime(2019, 2, 23, 3, 45, tzinfo=tzutc()), datetime(2019, 2, 23, 3, 30, tzinfo=tzutc()), datetime(2019, 2, 23, 3, 15, 1, tzinfo=tzutc()), datetime(2019, 2, 23, 3, 0, tzinfo=tzutc()), datetime(2019, 2, 23, 2, 45, tzinfo=tzutc()), datetime(2019, 2, 23, 2, 30, 1, tzinfo=tzutc()), datetime(2019, 2, 23, 2, 15, tzinfo=tzutc()), datetime(2019, 2, 23, 2, 0, tzinfo=tzutc()), datetime(2019, 2, 23, 1, 45, tzinfo=tzutc()), datetime(2019, 2, 23, 1, 30, tzinfo=tzutc()), datetime(2019, 2, 23, 1, 15, tzinfo=tzutc()), datetime(2019, 2, 23, 1, 0, tzinfo=tzutc()), datetime(2019, 2, 23, 0, 45, tzinfo=tzutc()), datetime(2019, 2, 23, 0, 30, tzinfo=tzutc()), datetime(2019, 2, 23, 0, 15, tzinfo=tzutc()), datetime(2019, 2, 23, 0, 0, tzinfo=tzutc()), datetime(2019, 2, 22, 23, 45, tzinfo=tzutc()), datetime(2019, 2, 22, 23, 30, tzinfo=tzutc()), datetime(2019, 2, 22, 23, 15, tzinfo=tzutc()), datetime(2019, 2, 22, 23, 0, tzinfo=tzutc()), datetime(2019, 2, 22, 22, 45, tzinfo=tzutc()), datetime(2019, 2, 22, 22, 30, tzinfo=tzutc()), datetime(2019, 2, 22, 22, 15, tzinfo=tzutc()), datetime(2019, 2, 22, 22, 0, 1, tzinfo=tzutc()), datetime(2019, 2, 22, 21, 45, tzinfo=tzutc()), datetime(2019, 2, 22, 21, 30, tzinfo=tzutc()), datetime(2019, 2, 22, 21, 15, tzinfo=tzutc()), datetime(2019, 2, 22, 21, 0, tzinfo=tzutc()), datetime(2019, 2, 22, 20, 45, 1, tzinfo=tzutc()), datetime(2019, 2, 22, 20, 30, tzinfo=tzutc()), datetime(2019, 2, 22, 20, 15, tzinfo=tzutc()), datetime(2019, 2, 22, 20, 0, tzinfo=tzutc()), datetime(2019, 2, 22, 19, 45, tzinfo=tzutc()), datetime(2019, 2, 22, 19, 30, tzinfo=tzutc()), datetime(2019, 2, 22, 19, 15, tzinfo=tzutc()), datetime(2019, 2, 22, 19, 0, tzinfo=tzutc()), datetime(2019, 2, 22, 18, 45, tzinfo=tzutc()), datetime(2019, 2, 22, 18, 30, tzinfo=tzutc()), datetime(2019, 2, 22, 18, 15, tzinfo=tzutc()), datetime(2019, 2, 22, 18, 0, tzinfo=tzutc()), datetime(2019, 2, 22, 17, 45, 1, tzinfo=tzutc())]

    levels = [0.107, 0.095, 0.104, 0.118, 0.119, 0.12, 0.121, 0.121, 0.122, 0.133, 0.141, 0.145, 0.135, 0.118, 0.105, 0.102, 0.1, 0.1, 0.101, 0.104, 0.109, 0.113, 0.12, 0.13, 0.144, 0.14, 0.116, 0.113, 0.112, 0.111, 0.114, 0.117, 0.118, 0.119, 0.12, 0.122,
0.123, 0.123, 0.123, 0.122, 0.121, 0.121, 0.12, 0.12, 0.118, 0.118, 0.119, 0.12, 0.12, 0.121, 0.122, 0.124, 0.125, 0.125, 0.126, 0.126, 0.126, 0.126, 0.125, 0.125, 0.124, 0.123, 0.122, 0.122, 0.122, 0.122, 0.123, 0.125, 0.127, 0.126, 0.126, 0.126, 0.126, 0.126, 0.125, 0.125, 0.124, 0.123, 0.123, 0.122, 0.122, 0.121, 0.121, 0.121, 0.121, 0.122, 0.122, 0.123, 0.122, 0.122, 0.122, 0.122, 0.121, 0.12]

    # Expected values
    coeffs_exp = [-0.19448883, -0.34247704, -0.20160748, -0.05613193, 0.11311839]
    d0_exp = 737113.7083333334

    # Run polyfit
    poly, d0 = polyfit(dates, levels, 4)

    # Check values line up
    assert round(poly.c[0] - coeffs_exp[0], 3) == 0
    assert round(poly.c[1] - coeffs_exp[1], 3) == 0
    assert round(poly.c[2] - coeffs_exp[2], 3) == 0
    assert round(poly.c[3] - coeffs_exp[3], 3) == 0
    assert round(poly.c[4] - coeffs_exp[4], 3) == 0
    assert round(d0 - d0_exp, 3) == 0

test_polyfit()