# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from haversine import haversine
from .utils import sorted_by_key  # noqa

def stations_by_distance(stations, p):
    """ Return list of stations and distance from a coordinated, sorted by the distance """
    stations_distance = []
    # Loop through all stations
    for station in stations:
        # Calculate distance between station and p
        distance = haversine(p, station.coord)
        # Create tuple of station and distance and add it to list
        stations_distance.append((station, distance))
    # Sort list and return it
    return sorted_by_key(stations_distance, 1)

def stations_within_radius(stations, centre, r):
    """ Return stations within a certain distance of a coordinate """
    stations_within_radius = []
    # Loop through all stations
    for station in stations:
        # Calculate distance between station and centre
        distance_from_centre = haversine(centre, station.coord)
        # Compares distance from centre to radius
        if distance_from_centre <= r:
            # Adds station to list if it is within the range
            stations_within_radius.append(station)
    return stations_within_radius

def rivers_with_station(stations):
    """ Return rivers that have a station associated with them """
    return stations_by_river(stations).keys()

def stations_by_river(stations):
    """ Return rivers and the stations that are associated with that river """
    rivers_stations = {}
    for station in stations:
        try:
            # Try add to dictionary
            rivers_stations[station.river].append(station)
        except:
            # If it fails, add list to dictionary
            rivers_stations[station.river] = [station]
    return rivers_stations

def rivers_by_station_number(stations, N):
    """ Return rivers sorted by number of stations, up to N stations. Returns more rivers if there are multiple with N stations. """
    # Get stations by river
    rivers_stations = stations_by_river(stations)
    # Create list of tuples with river and number of stations
    rivers_stations_number = [(river, len(stations)) for river, stations in rivers_stations.items()]
    sorted_list = sorted_by_key(rivers_stations_number, 1, True)

    # Check that N isn't higher than the list length
    if N + 1 > len(sorted_list):
        return sorted_list

    # Retrieve the number of stations for the Nth river
    number = sorted_list[N-1][1]

    for index in range(N, len(sorted_list)):
        if sorted_list[index][1] < number:
            return sorted_list[:index]

    return sorted_list