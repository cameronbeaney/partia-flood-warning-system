""" Methods related to plotting graphs about flood data """

import matplotlib.pyplot as plt
from matplotlib.dates import date2num
import numpy as np
from datetime import datetime, timedelta
from .analysis import polyfit

def plot_water_levels(station, dates, levels):
    """ Plot water level vs date for a station """
    plt.plot(dates, levels)
    
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title("Station " + station.name)

    plt.tight_layout() # This makes sure plot does not cut off date labels

    plt.show()

def plot_water_level_with_fit(station, dates, levels, p):
    """ Plot water level vs date for a station, with a best fit polynomial """

    # Evaluate polynomial
    poly, d0 = polyfit(dates, levels, p)

    # Plot polynomial and data
    plt.plot(dates, levels)

    # Plot polynomial fit at 30 points along interval (note that polynomial
    # is evaluated using the shift x)
    ds = date2num(dates)

    x1 = np.linspace(ds[0], ds[-1], 30)
    plt.plot(x1, poly(x1 - d0))

    # Plot typical range
    plt.axhline(y=station.typical_range[0])
    plt.axhline(y=station.typical_range[1])
    
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title("Station " + station.name)

    plt.tight_layout() # This makes sure plot does not cut off date labels

    plt.show()