""" Methods to analyse flood data from stations """

import numpy as np
from matplotlib.dates import date2num

def polyfit(dates, levels, p):
    """ Fit a polynomial to the dates and levels for a particular station """
    ds = date2num(dates)

    # Normalize values and store x0
    d0 = ds[0]
    ds = ds - d0

    # Fit polynomial
    coeffs = np.polyfit(ds, levels, p)

    # Create polynomial
    poly = np.poly1d(coeffs)

    return (poly, d0)