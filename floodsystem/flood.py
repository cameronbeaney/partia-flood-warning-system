"""
Contains functions related to flood warning
"""

from .utils import sorted_by_key

def stations_level_over_threshold(stations, tol):
    """ Return stations where the relative water level is over tol """
    tuples = []
    # Loop through stations
    for station in stations:
        wl = station.relative_water_level()
        if wl is not None and wl > tol:
            # Add tuple to list
            tuples.append((station, wl))

    # Sort list
    return sorted_by_key(tuples, 1, True)

def stations_highest_rel_level(stations, N):
    """ Return list of stations with highest relative water levels, sorted in descending order by relative water level """
    # Create list of stations with their relative levels
    stations_level = []
    for station in stations:
        wl = station.relative_water_level()
        if wl is not None:
            stations_level.append((station, wl))
    # Sort list in descending order
    stations_level = sorted_by_key(stations_level, 1, True)
    # Return first N
    return [x[0] for x in stations_level[:N]]

def towns_average_level(stations):
    """ Returns towns with the average relative water level """
    towns_stations = {}

    for station in stations:
        # Add the station to the towns_station dictionary
        try:
            towns_stations[station.town].append(station)
        except:
            towns_stations[station.town] = [station]
        
    towns_average_level = []
    for town, stations in towns_stations.items():
        # Calculate mean water level
        mean_level = 0
        number = 0
        for station in stations:
            if not station.relative_water_level() == None:
                mean_level += station.relative_water_level()
                number += 1
        if number > 0:
            mean_level /= number
            towns_average_level.append((town, mean_level))

    # Sort list of towns and mean level
    towns_average_level = sorted_by_key(towns_average_level, 1, True)

    return towns_average_level

def towns_flooding_risk(stations, N):
    """ Return list of towns and risk of flooding """
    # Get the level of flooding for each town
    towns_level = towns_average_level(stations)[:N]
    towns_risk = []
    # Assign Low, Moderate, High and Severe to each station
    for town, level in towns_level:
        risk = "Low"
        if level > 2:
            risk = "Severe"
        elif level > 1.5:
            risk = "High"
        elif level > 1:
            risk = "Moderate"
        towns_risk.append((town, risk))
    return towns_risk