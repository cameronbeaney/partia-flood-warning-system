from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance
from floodsystem.geo import stations_within_radius
from floodsystem.utils import sorted_by_key

def run():
    """Requirements for Task 1C"""

    # Build list of stations
    stations = build_station_list()
    stations_radius = stations_within_radius(stations, (52.2053,0.1218), 10)
    sorted_stations = sorted(stations_radius, key = lambda station: station.name)
    print("Stations within radius:")
    print(sorted_stations)


if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()