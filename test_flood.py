from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold
from floodsystem.flood import stations_highest_rel_level
from floodsystem.flood import towns_average_level
from floodsystem.flood import towns_flooding_risk

def test_stations_level_over_threshold():
    # Create test data
    stations = []
    stations.append(MonitoringStation("test-s-id-1", "test-m-id-1", "1", (0,1), (1,2), "", ""))
    stations.append(MonitoringStation("test-s-id-2", "test-m-id-2", "2", (0,2), (1,2), "", ""))
    stations.append(MonitoringStation("test-s-id-3", "test-m-id-3", "3", (0,1.5), (1,2), "", ""))
    stations.append(MonitoringStation("test-s-id-4", "test-m-id-4", "4", (0,-1.75), (1,2), "", ""))

    # Normal range
    stations[0].typical_range = (0, 5)
    stations[0].latest_level = 2.5

    # At zero
    stations[1].typical_range = (5, 10)
    stations[1].latest_level = 5

    # Outside typical range (over threshold)
    stations[2].typical_range = (0, 5)
    stations[2].latest_level = 10

    # Invalid typical range
    stations[3].typical_range = (0, -5)
    stations[3].latest_level = 2.5

    stations_threshold = stations_level_over_threshold(stations, 1)

    assert len(stations_threshold) == 1

def test_stations_highest_rel_level():
    # Create test data
    stations = []
    stations.append(MonitoringStation("test-s-id-1", "test-m-id-1", "1", (0,1), (1,2), "", ""))
    stations.append(MonitoringStation("test-s-id-2", "test-m-id-2", "2", (0,2), (1,2), "", ""))
    stations.append(MonitoringStation("test-s-id-3", "test-m-id-3", "3", (0,1.5), (1,2), "", ""))
    stations.append(MonitoringStation("test-s-id-4", "test-m-id-4", "4", (0,-1.75), (1,2), "", ""))

    # Normal range
    stations[0].typical_range = (0, 5)
    stations[0].latest_level = 2.5

    # At zero
    stations[1].typical_range = (5, 10)
    stations[1].latest_level = 5

    # Outside typical range (over threshold)
    stations[2].typical_range = (0, 5)
    stations[2].latest_level = 10

    # Invalid typical range
    stations[3].typical_range = (0, -5)
    stations[3].latest_level = 2.5

    stations_level = stations_highest_rel_level(stations, 2)

    assert stations_level[0].latest_level == 10
    assert stations_level[1].latest_level == 2.5

    assert len(stations_level) == 2

def test_towns_average_level():
    # Create test data
    stations = []
    stations.append(MonitoringStation("test-s-id-1", "test-m-id-1", "1", (0,1), (1,2), "", "town1"))
    stations.append(MonitoringStation("test-s-id-2", "test-m-id-2", "2", (0,2), (1,2), "", "town1"))
    stations.append(MonitoringStation("test-s-id-3", "test-m-id-3", "3", (0,1.5), (1,2), "", "town2"))
    stations.append(MonitoringStation("test-s-id-4", "test-m-id-4", "4", (0,-1.75), (1,2), "", "town3"))

    # Add flood level data
    # relative level of 0.5
    stations[0].latest_level = 4.5
    stations[0].typical_range = (2, 7)
    # relative level of 1
    stations[1].latest_level = 5
    stations[1].typical_range = (0, 5)
    # relative level of 3
    stations[2].latest_level = 9
    stations[2].typical_range = (0, 3)
    # relative level of 1.75
    stations[3].latest_level = 1.75
    stations[3].typical_range = (0, 1)

    towns_level = towns_average_level(stations)
    
    assert towns_level[0][1] == 3
    assert towns_level[1][1] == 1.75
    assert towns_level[2][1] == 0.75
    assert len(towns_level) == 3

def test_towns_flooding_risk():
    # Create test data
    stations = []
    stations.append(MonitoringStation("test-s-id-1", "test-m-id-1", "1", (0,1), (1,2), "", "town1"))
    stations.append(MonitoringStation("test-s-id-2", "test-m-id-2", "2", (0,2), (1,2), "", "town1"))
    stations.append(MonitoringStation("test-s-id-3", "test-m-id-3", "3", (0,1.5), (1,2), "", "town2"))
    stations.append(MonitoringStation("test-s-id-4", "test-m-id-4", "4", (0,-1.75), (1,2), "", "town3"))

    # Add flood level data
    # relative level of 0.5
    stations[0].latest_level = 4.5
    stations[0].typical_range = (2, 7)
    # relative level of 1
    stations[1].latest_level = 5
    stations[1].typical_range = (0, 5)
    # relative level of 3
    stations[2].latest_level = 9
    stations[2].typical_range = (0, 3)
    # relative level of 1.75
    stations[3].latest_level = 1.75
    stations[3].typical_range = (0, 1)

    towns_level = towns_flooding_risk(stations, 2)

    assert towns_level[0][1] == "Severe"
    assert towns_level[1][1] == "High"
    assert len(towns_level) == 2

test_towns_average_level()
test_towns_flooding_risk()