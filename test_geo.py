"""Unit test for the geo module"""

from floodsystem.geo import stations_by_distance
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number
from floodsystem.geo import stations_within_radius
from floodsystem.station import MonitoringStation

def test_stations_by_distance():
    # Create test data
    stations = []
    stations.append(MonitoringStation("test-s-id-1", "test-m-id-1", "1", (0,1), (1,2), "", ""))
    stations.append(MonitoringStation("test-s-id-2", "test-m-id-2", "2", (0,2), (1,2), "", ""))
    stations.append(MonitoringStation("test-s-id-3", "test-m-id-3", "3", (0,1.5), (1,2), "", ""))
    stations.append(MonitoringStation("test-s-id-4", "test-m-id-4", "4", (0,-1.75), (1,2), "", ""))

    # Sort stations by distance
    p = (0,0)
    stations_distance = stations_by_distance(stations, p)
    
    # Run tests
    assert stations_distance[0][0].coord == (0,1)
    assert stations_distance[1][0].coord == (0,1.5)
    assert stations_distance[2][0].coord == (0,-1.75)
    assert stations_distance[3][0].coord == (0,2)

    # Test distances
    assert round(stations_distance[0][1], 1) == 111.2

def test_rivers_with_station():
    # Create test data
    stations = []
    stations.append(MonitoringStation("test-s-id-1", "test-m-id-1", "1", (0,1), (1,2), "river-1", ""))
    stations.append(MonitoringStation("test-s-id-2", "test-m-id-2", "2", (0,2), (1,2), "river-2", ""))
    stations.append(MonitoringStation("test-s-id-3", "test-m-id-3", "3", (0,1.5), (1,2), "river-2", ""))
    stations.append(MonitoringStation("test-s-id-4", "test-m-id-4", "4", (0,-1.75), (1,2), "river-3", ""))

    # Get rivers
    rivers = rivers_with_station(stations)

    assert len(rivers) == 3
    
def test_stations_by_river():
    # Create test data
    stations = []
    stations.append(MonitoringStation("test-s-id-1", "test-m-id-1", "1", (0,1), (1,2), "river-1", ""))
    stations.append(MonitoringStation("test-s-id-2", "test-m-id-2", "2", (0,2), (1,2), "river-2", ""))
    stations.append(MonitoringStation("test-s-id-3", "test-m-id-3", "3", (0,1.5), (1,2), "river-2", ""))
    stations.append(MonitoringStation("test-s-id-4", "test-m-id-4", "4", (0,-1.75), (1,2), "river-3", ""))

    # Get stations by river
    stations_rivers = stations_by_river(stations)

    assert len(stations_rivers) == 3
    assert len(stations_rivers["river-1"]) == 1
    assert len(stations_rivers["river-2"]) == 2
    assert len(stations_rivers["river-3"]) == 1

def test_rivers_by_station_number():
     # Create test data
    stations = []
    stations.append(MonitoringStation("test-s-id-1", "test-m-id-1", "1", (0,1), (1,2), "river-1", ""))
    stations.append(MonitoringStation("test-s-id-2", "test-m-id-2", "2", (0,2), (1,2), "river-2", ""))
    stations.append(MonitoringStation("test-s-id-3", "test-m-id-3", "3", (0,1.5), (1,2), "river-2", ""))
    stations.append(MonitoringStation("test-s-id-4", "test-m-id-4", "4", (0,-1.75), (1,2), "river-3", ""))

    rivers_stations_number = rivers_by_station_number(stations, 1)

    # Test the functionality that means that extra rivers with the same number of stations will be returned
    assert rivers_stations_number[0][1] == 2

    # General tests
    rivers_stations_number = rivers_by_station_number(stations, 2)
    assert len(rivers_stations_number) == 3

def test_stations_within_radius():
     # Create test data
    stations = []
    stations.append(MonitoringStation("test-s-id-1", "test-m-id-1", "1", (0,1), (1,2), "", ""))
    stations.append(MonitoringStation("test-s-id-2", "test-m-id-2", "2", (0,2), (1,2), "", ""))
    stations.append(MonitoringStation("test-s-id-3", "test-m-id-3", "3", (0,1.5), (1,2), "", ""))
    stations.append(MonitoringStation("test-s-id-4", "test-m-id-4", "4", (0,-1.75), (1,2), "", ""))

    # Get stations within radius
    p = (0,0)
    stations_radius = stations_within_radius(stations, p, 175)
    
    # Check only two are returned
    assert len(stations_radius) == 2