from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_with_station

def run():
    """Requirements for Task 1D"""

    # Build list of stations
    stations = build_station_list()

    rivers = rivers_with_station(stations)
    # Print number of rivers
    print("Number of rivers with at least one station:", len(rivers))
    # Print first ten stations
    print("First ten:", sorted(rivers)[:10])

    # Rivers to show stations for
    rivers_to_test = ["River Aire", "River Cam", "River Thames"]
    stations_rivers = stations_by_river(stations)
    for river_to_test in rivers_to_test:
        print(river_to_test + ":")
        try:
            # Extract the station names and sort them
            print(sorted([station.name for station in stations_rivers[river_to_test]]))
        except:
            print("No stations for this river")


if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()