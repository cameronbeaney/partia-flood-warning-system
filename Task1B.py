from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance

def run():
    """Requirements for Task 1B"""

    # Build list of stations
    stations = build_station_list()
    stations_distance = stations_by_distance(stations, (52.2053,0.1218))

    # Print closest 10
    print("Closest 10:")
    print(stations_distance[:10])

    # Print furthest 10
    print("Furthest 10:")
    print(stations_distance[-10:])


if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()