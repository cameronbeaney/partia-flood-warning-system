from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_level_with_fit

import datetime

def run():
    # Build list of stations
    stations = build_station_list()

    # Update water levels
    update_water_levels(stations)

    # Get 5 stations with highest relative levels
    stations_relative = stations_highest_rel_level(stations, 5)

    dt = 2

    for station in stations_relative:
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        plot_water_level_with_fit(station, dates, levels, 4)


if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    run()
