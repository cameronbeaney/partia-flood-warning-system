from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_highest_rel_level


def run():
    # Build list of stations
    stations = build_station_list()

    # Update water levels
    update_water_levels(stations)

    # Get 10 stations with highest relative levels
    stations_relative = stations_highest_rel_level(stations, 10)

    for station in stations_relative:
        print(station.name, station.relative_water_level())


if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    run()
